#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Hevelli - Template system for file system trees
# http://www.florian-diesch.de/software/hevelli/
#
# Copyright (C) 2017 Florian Diesch <devel@florian-diesch.de>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os.path, sys

fullpath = os.path.abspath(__file__)
path = os.path.split(fullpath)[0]
sys.path=[path]+sys.path

import hevelli, hevelli.settings

DATA_DIR=os.path.normpath(os.path.join(path, 'data'))

hevelli.settings.DATA_DIR = DATA_DIR
hevelli.settings.UI_DIR = os.path.join(DATA_DIR, 'ui')
hevelli.settings.ICON_DIR = os.path.join(DATA_DIR, 'icons')


hevelli.main()
