#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Hevelli - Template system for file system trees
# http://www.florian-diesch.de/software/hevelli/
#
# Copyright (C) 2017 Florian Diesch <devel@florian-diesch.de>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from setuptools import setup, find_packages

setup(
    name='hevelli',
    version=0.1,
    packages=find_packages(),
    include_package_data=True,
    maintainer='Florian Diesch',
    maintainer_email='devel@florian-diesch.de',
    author = "Florian Diesch",
    author_email = "devel@florian-diesch.de",
    description='Template system for file system trees',
    long_desc=\
"""
=========
 hevelli
=========
""",     
    data_files=[
        ('/usr/share/applications',
         glob.glob('data/desktop/*.desktop')),
        ('share/hevelli/ui/',
         glob.glob('data/ui/*.ui')),
        ('share/hevelli/icons/',
         glob.glob('data/icons/*.png')),
        ('share/icons/hicolor/scalable/apps/',
         glob.glob('data/icons/*.svg')),
         ],
    entry_points = {
        'console_scripts': ['hevelli=hevelli:main'],
        },
    license='GPLv3',
    url='http://www.florian-diesch.de/software/hevelli/',
    download_url='http://www.florian-diesch.de/software/hevelli/',
    keywords = "",
    classifiers=[
        ''
        ],
    )
