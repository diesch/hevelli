# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
import pytest

from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource
from hevelli.fileparser import FileParser
from hevelli.world import World
from hevelli.ui import SimpleTextUI

import hevelli.errors as errors

def test_attributes():
    parser = FileParser(None, None)
    for attr in 'path', 'text', 'copy', 'template', 'perm':
        assert attr in parser.attributes
        assert attr in parser.defaults


def test_get_text_string():
    parser = FileParser(None, None)
    assert parser._get_text_string(['']) == '\n'
    assert parser._get_text_string(['bla', 'foo']) == 'bla\nfoo\n'


def test_get_copy_string(tmpdir):
    text = 'some\ntext\n'
    txtfile = tmpdir.join('test.txt')
    txtfile.write(text)
    
    parser = FileParser(None, None)    
    assert parser._get_copy_string([str(txtfile)]) == text


def test_get_template_string(tmpdir):
    txtfile = tmpdir.join('test.txt')
    txtfile.write('some {{var}}')
    world = World(None)
    world.set_var('var', 'test blah')
    
    parser = FileParser(None, world)
    assert parser._get_template_string([str(txtfile)]) == "some test blah"


class Test_on_end():

  @pytest.fixture()
  def textfiles(self, tmpdir):
      for i in range(5):
        txtfile = tmpdir.join('text{}.txt'.format(i))
        txtfile.write('Text %s {{monty}}\n' % i)
      return tmpdir


  testdata = [
    ('out.txt', dedent("""\
        path = 'out.txt'
      end"""),
     ""),

    ('out.txt', dedent("""
        path = 'out.txt'
        text = 'some text'
       end
     """),
     "some text\n"),

    ('out.txt', dedent("""
        path = 'out.txt'
        text = 'some text'
        text = 'more text'
       end
     """),
     dedent("""\
       some text
       more text
     """)),

     ('out.txt', dedent("""
        path = 'out.txt'
        text = 'some text'
        copy = 'text1.txt'
        text = 'more text'
        copy = 'text2.txt'
        text = '***' 
        template = 'text3.txt'       
       end
     """),
     dedent("""\
       some text
       Text 1 {{monty}}
       more text
       Text 2 {{monty}}
       ***
       Text 3 python""")), 
      ]
  @pytest.mark.parametrize('out_path, content, expected', testdata)     
  def test_all(self, out_path, content, expected, textfiles):
      textfiles.chdir()
      outfile = MagicMock()
      world = World(None)
      
      world.set_var('monty', 'python')
      text = StringIO(content)
      with InputSource(text) as inp:
          parser = FileParser(inp, world)          
          parser._get_output_file = MagicMock(
              return_value=outfile)
          parser.parse()

          parser._get_output_file.assert_called_with(out_path)
          outfile = outfile.__enter__()
          written = ''.join(
              arg[0][0] for arg in outfile.write.call_args_list)
          assert written == expected

      

    
    
        




        


