# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
import pytest
from textwrap import dedent
import os.path

import hevelli.errors as errors
import hevelli.utils as utils
import hevelli.settings as settings
import hevelli.inputsources as inputsources


testdata = [
    ('echo foo', 'foo\n'),
    ('/bin/true', ''),
    ('printf blah', 'blah'),
    ]
@pytest.mark.parametrize('cmd, output', testdata)   
def test_run_shell_command(cmd, output):
    inputsrc = inputsources.InputSource(file='test')
    assert utils.run_shell_command(cmd, inputsrc) == output


testdata = [
    ('false', errors.ExecError),
    ('/doesntexist', errors.ExecError),
    ]
@pytest.mark.parametrize('cmd, error', testdata)   
def test_run_shell_command__error(cmd, error):
    inputsrc = inputsources.InputSource(file='test')
    with pytest.raises(error):
        utils.run_shell_command(cmd, inputsrc)

        
testdata = [
    (('./sub1/.hevelli/', './sub1/sub2/.hevelli/', './.hevelli/ROOT'),
     './sub1/sub2', 
     ['.hevelli', 'sub1/.hevelli', 'sub1/sub2/.hevelli']
     ),
    (('./sub1/.hevelli/', './sub1/sub2/.hevelli/', '.sub1/.hevelli/ROOT'),
     './sub1/sub2', 
     ['sub1/.hevelli', 'sub1/sub2/.hevelli']
     ),
     
    ]
@pytest.mark.parametrize('pathes, pwd, expected', testdata)   
def test_get_config_dirs(tmpdir, pathes, pwd, expected):
    tmpdir.chdir()
    for p in pathes:
        tmpdir.ensure(p, dir=p.endswith('/'))
    tmpdir.join(pwd).chdir()
    assert [os.path.relpath(i, str(tmpdir)) for i in utils.get_config_dirs()] == expected


testdata = [
    (('./.hevelli/attributes.json',
      './sub1/sub2/sub3/.hevelli/settings.json',
      './sub1/sub2/sub3/.hevelli/attributes.json',
     ),
     ['.hevelli/attributes.json',
      'sub1/sub2/sub3/.hevelli/attributes.json'  
     ],
     './sub1/sub2/sub3/sub4/'
     ),
     
    ]
@pytest.mark.parametrize('pathes, expected, pwd', testdata)   
def test_get_config_files(tmpdir, pathes, pwd, expected):
    tmpdir.chdir()
    for p in pathes:
        tmpdir.ensure(p)
    tmpdir.join(pwd).ensure(dir=True).chdir()
    assert [os.path.relpath(i, str(tmpdir)) for i in utils.get_config_files('attributes.json')] == expected
