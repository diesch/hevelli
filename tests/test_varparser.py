#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
import pytest

from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource
from hevelli.varparser import VarParser
from hevelli.world import World
from hevelli.ui import SimpleTextUI

import hevelli.errors as errors


testdata = [
    ('', 'width', '3', 'Value for "width"'),
    ('How long', 'width', '3', 'How long'),
    ]
@pytest.mark.parametrize('question, name, value, expected', testdata)   
def test_get_question_msg(question, name, value, expected):
    vp = VarParser( None, None)
    
    s = vp._get_question_msg(question, name, value)
    assert s == expected

        
class Test_maybe_ask(unittest.TestCase):

    def setUp(self):
        self.world = World(None)

    def test_ask_never(self):
        self.world.ui = MagicMock()
        vp = VarParser(None, world=self.world)
        vp.data = [{'key': 'ask', 'value': ['never']}]
        
        value = vp._maybe_ask('foobar')
        self.world.ui.ask.assert_not_called()
        assert value == 'foobar'

        value = vp._maybe_ask('')
        self.world.ui.ask.assert_not_called()
        assert value == ''
        

    def test_ask_auto_nonempty_value(self):
        self.world.ui = MagicMock()
        vp = VarParser(None, world=self.world)
        vp.data = [{'key': 'ask', 'value': ['auto']}]
                
        value = vp._maybe_ask('foobar')
        self.world.ui.ask.assert_not_called()
        assert value == 'foobar'      
    

    def test_ask_auto_empty_value(self):
        self.world.ui = MagicMock()
        self.world.ui.ask.return_value = 'blah'
        vp = VarParser( None, world=self.world)
        vp.data = [{'key': 'ask', 'value': ['auto']},
                   {'key': 'name', 'value': ['title']},
                   {'key': 'question', 'value': ['']}]
                
        value = vp._maybe_ask('')
        
        msg = vp._get_question_msg('', 'title', '')
        self.world.ui.ask.assert_called_once_with(msg, '')
        assert value == 'blah'
        
    def test_ask_always_empty_value(self):
        self.world.ui = MagicMock()
        self.world.ui.ask.return_value = 'blah'
        vp = VarParser(None, world=self.world)
        vp.data = [{'key': 'ask', 'value': ['always']},
                   {'key': 'name', 'value': ['title']},
                   {'key': 'question', 'value': ['']}]
                
        value = vp._maybe_ask('')
        msg = vp._get_question_msg('', 'title', '')
        self.world.ui.ask.assert_called_once_with(msg, '')
        assert value == 'blah'
        
    def test_ask_always_nonempty_value(self):
        self.world.ui = MagicMock()
        self.world.ui.ask.return_value = 'blah'
        vp = VarParser(None, world=self.world)
        vp.data = [{'key': 'ask', 'value': ['always']},
                   {'key': 'name', 'value': ['title']},
                   {'key': 'question', 'value': ['']}]
        
        value = vp._maybe_ask('foobar')
        msg = vp._get_question_msg('', 'title', 'foobar')
        self.world.ui.ask.assert_called_once_with(msg, 'foobar')
        assert value == 'blah'



testdata = [
    (dedent("""\
        name = 'foo' 
        value  = 'bar'
        question = 'What's foo?'
      end
      """),
      {'name': 'foo',
       'value': 'bar',
       'ask': 'auto',
       'question': "What's foo?"
      }
    ),

    (dedent("""\
       name = 'foo' 
       value  = 'bar'
       ask = 'auto'
       question = 'What's foo?'
     end
    """),
    {
       'name': 'foo',
       'value': 'bar',
       'ask': 'auto',
       'question': "What's foo?"
    }
    ),

    (dedent("""\
       name = 'joe's' 
       value = 'Grill'
       value = ' & '
       value = 'Bar'
       ask = 'auto'
       question = 'This is'
       question = 'multiline'
      end
      """),
      {
        'name': "joe's",
        'value': 'Grill\n & \nBar',
        'ask': 'auto',
        'question': "This is\nmultiline"
      }
      )
]
@pytest.mark.parametrize('content, items', testdata)
def test_parse(content, items):
     text = StringIO(content)
     with InputSource(text) as inp:
         world = World(None)
         parser = VarParser(inp, world)
         parser.parse()
        
         assert inp.line == 'end'
         
         for key, value in items.items():
             assert parser.get_attrib_value(key) == value


             
testdata = [
    (errors.InvalidAttributeError, 3,
     dedent("""\
     name = 'bla'
     invalid = 'foo'
    end
    """)),

    (errors.ParserError, 3,
     dedent("""\
     name = 'test'
     ask     
    end
    """)),

    (errors.ParserError, 3,
     dedent("""\
     name = 'test'
     ask =    
    end
    """)),

    (errors.InvalidValueError, 3,
     dedent( """\
     name = 'test'
     ask = 'hm'
    end
    """)),

    (errors.InvalidValueError, 2,
     dedent("""\
     name = test
     ask = 'hm'
    end
    """)),

    (errors.InvalidValueError, 2,
     dedent( """\
     name = "test
     ask = 'hm'
    end
    """)),

    (errors.RequiredAttributeMissingError, 3,
     dedent("""\
     value = 'hm'
    end
    """)),
]
@pytest.mark.parametrize('error, line_num, content', testdata)
def test_parse_error(error, line_num, content):
    text = StringIO(content)
        
    with InputSource(text) as inp:
        world = World(None)
        parser = VarParser(inp, world)
        with pytest.raises(error) as cm:
            parser.parse()
            lnum = cm.exception.inputsrc.line_num
            assert line_num == lnum
                

testdata = [
    ('joe',
     'Grill\n & \nBar',
     dedent("""\
       name = 'joe' 
       value = 'Grill'
       value = ' & '
       value = "Bar"
      end
      """),
    ),
    ('foo',
      '',
      dedent("""
       name = 'foo'
      end
      """),
    ),
]    
@pytest.mark.parametrize('name, value, content', testdata)
def test_all(name, value, content):
     text = StringIO(content)
     with InputSource(text) as inp:
        world = World(None)
        parser = VarParser(inp, world)
        parser.parse()
        assert world.get_var(name) == value
        

        
