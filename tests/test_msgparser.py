# -*- coding: utf-8 -*-


import unittest
from unittest.mock import MagicMock
import pytest

from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource
from hevelli.msgparser import MsgParser
from hevelli.world import World
from hevelli.ui import SimpleTextUI

import hevelli.errors as errors


def test_attributes():
    parser = MsgParser(None, None)
    assert 'text' in parser.attributes
    assert 'text' in parser.defaults


def test_on_end():
    ui = SimpleTextUI()
    ui.text = MagicMock()
    
    text = StringIO(" text = 'Hallo'\ntext = 'Welt!'\nend")    
    world = World(ui=ui)
    
    with InputSource(text) as inp:
        parser = MsgParser(inp, world)
        parser.parse()

        ui.text.assert_called_with('Hallo\nWelt!\n')

