# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
import pytest

import datetime
from textwrap import dedent

from hevelli.world import World
import hevelli.settings as settings
import hevelli.errors as errors
import hevelli.utils as utils
from hevelli.ui import SimpleTextUI


def text_from_call_args_list(mock):
    return '\n'.join(' '.join(c[0]) for c in mock.call_args_list)


def test_choice__simple():
    ui = SimpleTextUI()
    ui._print = MagicMock()
    ui._input = MagicMock(return_value='1')
    val = ui.choice('Test', [('a', 'A'), ('b', 'B'), ('c', 'C')])
    assert val == 'B'
    assert text_from_call_args_list(ui._print) == '\n'.join((
      'Test',
      '',
      '  0)  a',
      '  1)  b',
      '  2)  c',
        ))

def test_choice__error():
    ui = SimpleTextUI()
    ui._print = MagicMock()
    ui._input = MagicMock(side_effect=['a', '99', '0'])
    val = ui.choice('Test', [('a', 'A'), ('b', 'B'), ('c', 'C')])
    assert val == 'A'
    assert text_from_call_args_list(ui._print) == '\n'.join((
     'Test',
     '',
     '  0)  a',
     '  1)  b',
     '  2)  c',
     'Invalid choice: "a"',
     '',
     '',
     'Test',
     '',
     '  0)  a',
     '  1)  b',
     '  2)  c',
     'Invalid choice: "99"',
     '',
     '',
     'Test',
     '',
     '  0)  a',
     '  1)  b',
     '  2)  c',
    ))

    

def test_ask():
    ui =  SimpleTextUI()
    ui._input = MagicMock(return_value='blah')
    value = ui.ask('What?')

    ui._input.assert_called_once_with('What? [default: ] ')
    assert value == 'blah'

def test_ask__default():
    ui =  SimpleTextUI()
    ui._input = MagicMock(return_value='')
    value = ui.ask('What?', default='Orange')

    ui._input.assert_called_once_with('What? [default: Orange] ')
    assert value == 'Orange'


def test_confirm__yes():
    ui = SimpleTextUI()
    ui._input = MagicMock(side_effect=['a', '', 'y'])
    val = ui.confirm('Delete universe')
    assert val == True
    assert text_from_call_args_list(ui._input) == '\n'.join((
     'Delete universe? [y/n] ', 
     'Delete universe? [y/n] ', 
     'Delete universe? [y/n] ',
    ))

def test_confirm__no():
    ui = SimpleTextUI()
    ui._input = MagicMock(side_effect=['1', 'n'])
    val = ui.confirm('Delete universe')
    assert val == False
    assert text_from_call_args_list(ui._input) == '\n'.join((
     'Delete universe? [y/n] ', 
     'Delete universe? [y/n] ',
    ))
