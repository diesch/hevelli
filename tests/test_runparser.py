# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
import pytest

from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource
from hevelli.runparser import RunParser
from hevelli.world import World
from hevelli.ui import SimpleTextUI

import hevelli.errors as errors
import subprocess



def test_attributes():
    parser = RunParser(None, None)
    assert 'cmd' in parser.attributes
    assert 'cmd' in  parser.defaults
    assert 'quiet' in parser.attributes
    assert 'quiet' in  parser.defaults
    
def test_on_end_not_quiet():
    text = StringIO("cmd = '/bin/true'\nend")
    with unittest.mock.patch('subprocess.check_output'), \
         unittest.mock.patch('subprocess.check_call'):
        with InputSource(text) as inp:
            parser = RunParser(inp, None)
            parser.parse()

            subprocess.check_call.assert_called_with('/bin/true', shell=True)
            subprocess.check_output.assert_not_called()
        

def test_on_end_quiet():
    text = StringIO("cmd = '/bin/true'\nquiet = 'true'\nend")
    with unittest.mock.patch('subprocess.check_output'), \
         unittest.mock.patch('subprocess.check_call'):
        with InputSource(text) as inp:
            parser = RunParser(inp, None)
            parser.parse()

            subprocess.check_output.assert_called_with(
                '/bin/true', shell=True)
            subprocess.check_call.assert_not_called()


def test_on_end_error():
    text = StringIO("cmd = '/bin/true'\nend")
    with unittest.mock.patch('subprocess.check_output'), \
         unittest.mock.patch('subprocess.check_call', side_effect=OSError):
        with InputSource(text) as inp:
            parser = RunParser(inp, None)
            with pytest.raises(errors.ExecError):
                parser.parse()

            subprocess.check_call.assert_called_with(
                '/bin/true', shell=True)
            subprocess.check_output.assert_not_called()
    
    

