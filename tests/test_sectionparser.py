# -*- coding: utf-8 -*-
import unittest
from unittest.mock import MagicMock
import pytest

from io import StringIO
from textwrap import dedent

from hevelli import errors
from hevelli.inputsources import InputSource
from hevelli.parser import SectionParser
from hevelli.varparser import VarParser
from hevelli.attributes import Attrib


testdata = [
    ('=', "'blah'", 'blah'),
    ('<', "'echo blubb'", 'blubb\n'),
    ('<', "'true'", ''),
    ('<', "'printf blah'", 'blah'),
    ]
@pytest.mark.parametrize('op, value, expected', testdata)   
def test_add_data__single_line(op, value, expected):
    attrib = Attrib('var')
    parser = SectionParser(None, None)
    assert parser.data == []
    
    parser._add_data(attrib, op, value)
    assert parser.data[-1] == {'key': 'var', 'value': [expected]}
    assert parser.keys == {'var'}

testdata = [
    ('=', "'blah'", ['first', 'blah']),
    ('<', "'echo blubb'", ['first', 'blubb\n']),
    ('<', "'true'", ['first', '']),
    ('<', "'printf blah'", ['first', 'blah']),
    ]
@pytest.mark.parametrize('op, value, expected', testdata)   
def test_add_data__multi_line(op, value, expected):
    attrib = Attrib('var', is_multiline=True)
    parser = SectionParser(None, None)
    assert parser.data == []

    parser.data.append( {'key': 'var', 'value': ['first']})
    parser._add_data(attrib, op, value)
    assert parser.data[-1] == {'key': 'var', 'value': expected}
    assert parser.keys == {'var'}


testdata = [
    ('=', "blah'",  errors.InvalidValueError),
    ('<', "'false'", errors.ExecError),
    ]
@pytest.mark.parametrize('op, value, error', testdata)   
def test_add_data__error(op, value, error):
    attrib = Attrib('var')
    parser = SectionParser(None, None)
    assert parser.data == []

    with pytest.raises(error) as e:
        parser._add_data(attrib, op, value)

def test_add_data__only_once():
    attrib = Attrib('var', only_once=True)
    parser = SectionParser(None, None)

    parser._add_data(attrib, '=', "'test'")
    with pytest.raises(errors.InvalidAttributeError) as e:
        parser._add_data(attrib, '=', "'foo'")

        
def test_empty_input():
    text = StringIO("")  
    with InputSource(text) as inp:  
        parser = SectionParser(inp, None)
        with pytest.raises(errors.ParserError) as e:
            parser.parse()

       
def test_no_end():
    attrib = Attrib('foo')
    text = StringIO("foo = 'bar'")  
    with InputSource(text) as inp:  
        parser = SectionParser(inp, None)
        parser.attributes = {attrib.name: attrib}
        with pytest.raises(errors.ParserError) as e:
            parser.parse()

        
