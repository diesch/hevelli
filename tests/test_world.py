# -*- coding: utf-8 -*-


import unittest
from unittest.mock import MagicMock
import pytest

import datetime
from textwrap import dedent

from hevelli.world import World
import hevelli.settings as settings
import hevelli.errors as errors
import hevelli.utils as utils

def test_set_default_vars_from_dict():
    world = World(None)

    d = {
        "license": "GPL-3.0+", 
        "version": "0.01", 
        }
    world._set_default_vars_from_dict(d)
    assert world.vars_to_save == set()
    assert world.vars == d


def test_load_default_attributes_error(tmpdir):
    f = tmpdir.mkdir(settings.CONFIG_DIR).join(settings.ATTRIBUTES_FILE)
    data = 'blah'
    f.write(data)
    world = World(None)
    with pytest.raises(errors.ConfigFileError):
        world.load_default_attributes()

def test_load_default_attributes(tmpdir):
    f = tmpdir.mkdir(settings.CONFIG_DIR).join(settings.ATTRIBUTES_FILE)
    data = """{
       "license": "GPL-3.0+", 
       "version": "0.01"
    }"""
    f.write(data)
    tmpdir.chdir()
    world = World(None)
    world.load_default_attributes()
    assert world.vars_to_save == set()
    assert world.vars == {
        "license": "GPL-3.0+", 
        "version": "0.01",
    }
    

