#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock

from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource 
from hevelli.parser import RecipeParser
import hevelli.errors as errors



class ErrorMixin:
 
    def test_error(self):
        text = StringIO(self.content)
        def dummy(*args): return MagicMock()          
        parser = RecipeParser(
            None, {k:dummy for k in self.keys}) 
        with InputSource(text) as inp:
            with self.assertRaises(errors.ParserError) as cm:
                parser.parse(inp)

            e = cm.exception
            self.assertIs(e.inputsrc, inp)
            self.assertEqual(e.inputsrc.line_num, self.line_num)
            self.assertEqual(e.inputsrc.line, self.line)
            

class Unknown(ErrorMixin, unittest.TestCase):

    content = """
    file bla
    """

    keys = ['test']
    line_num=2
    line='file bla'
    
class KnownAndUnknown(ErrorMixin, unittest.TestCase):

    content = """
    test foo
    file bla
    """

    keys = ['test']
    line_num=3
    line='file bla'
    
class NoArgs(ErrorMixin, unittest.TestCase):

    content = """
    test
    file
    """

    keys = ['test']
    line_num=3
    line='file'

    
class RecipeMixin:
 
    def test_recipe(self):
        text = StringIO(self.content)
        section_parsers = {k:MagicMock() for k in self.keys}
        world = object()
        parser = RecipeParser(world, section_parsers)
        
        inp = InputSource(text)
        with inp:
            parser.parse(inp)

        for key, mock in section_parsers.items():
            self.assertEqual(mock.call_count, self.keys[key])
            mock.assert_called_with(inp, world)
            

        
class WithEnd(RecipeMixin, unittest.TestCase):

    content = """
    test
    end
    test
    file
    test
    end
    """

    keys = {'test':3, 'file':1, 'end':2}
        
            
