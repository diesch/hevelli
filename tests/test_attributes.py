#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest
from unittest.mock import MagicMock
from unittest import TestCase

import hevelli.attributes as attributes
import hevelli.errors as errors
from hevelli.world import World


testdata = [
    (None, r'(["\'].*["\'])\s*'),
    (   1, r'(["\'].*["\'])\s*'),
    (   2, r'(["\'].*["\'])\s*(["\'].*["\'])\s*'),
    ]
@pytest.mark.parametrize('num_args,expected', testdata)
def test_create_regex(num_args, expected):
        if num_args is None:
            a = attributes.Attrib('foo')
        else:
            a = attributes.Attrib('foo', num_args=num_args)
        assert a.create_regex() == expected


@pytest.fixture
def dummy_world():
    return World(None)

testdata =  [
        ('"blah\'',),
        ('\'blah"',),
        ('blah"'),
        ('\'blah'),
]       
@pytest.mark.parametrize('s', testdata) 
def test_parens_dont_match(s, dummy_world):
    a = attributes.Attrib('foo')
    with pytest.raises(ValueError):
        a.maybe_render_template(s, dummy_world)

            
def test_invalid_parens(dummy_world):
    a = attributes.Attrib('foo')
    with pytest.raises(ValueError):
        a.maybe_render_template('blah', dummy_world)

        
testdata = [("'{{foo}}'", '{{foo}}'),
            ("'foo'", 'foo'), 
            ("''", ''), 
]
@pytest.mark.parametrize('s,expected', testdata)         
def test_plain_string(s, expected, dummy_world):
        a = attributes.Attrib('blah')
        assert a.maybe_render_template(s, dummy_world) == expected

        
testdata = [
    ('"{{foo}}"', 'FOO'),
    ('"foo"', 'foo'),
    ('""', ''),
    ('"{{foo}}{{blah}}"', 'FOOBLAH'),
]
@pytest.mark.parametrize('s,expected', testdata)         
def test_template(s, expected, dummy_world):
    dummy_world.vars = {'foo': 'FOO', 'blah': 'BLAH'}
    a = attributes.Attrib('blah')
    assert a.maybe_render_template(s, dummy_world) == expected

testdata = [
    ("'foo'", 1, None, 'foo'),    
    ("'foo'", 2, ValueError, 'foo'),
    ("'foo' 'bar'", 1, None, "foo' 'bar"),
    ("'foo' 'bar'", 2, None, ['foo', 'bar']),

]
@pytest.mark.parametrize('value, num_args, exception, expected', testdata)
def test_parse(value, num_args, exception, expected, dummy_world):
    a = attributes.Attrib('blah', num_args=num_args)
    if exception is None:
        assert a.parse(value, dummy_world) == expected
    else:
        with pytest.raises(exception):
            assert a.parse(value, dummy_world) == expected
