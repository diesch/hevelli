#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from io import StringIO
from textwrap import dedent

from hevelli.inputsources import InputSource 


class InputMixin:
 
    def test_input(self):
        text = StringIO(self.content)
        
        with InputSource(text) as inp:
            outp = '\n'.join(i for i in inp)
            #outp = ''
        self.assertEqual(outp, self.expected)



class Plain(InputMixin, unittest.TestCase):
    content = """
    file bla
     name
    end
    """

    expected = dedent("""\
    file bla
    name
    end""")

    
class Comment(InputMixin, unittest.TestCase):
    content = """
    # test
    file bla
     name
     # bla
    end
    #end
    """

    expected = dedent("""\
    file bla
    name
    end""")


    

if __name__ == '__main__':
    unittest.main()
