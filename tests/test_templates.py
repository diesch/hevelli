# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock

from io import StringIO
from textwrap import dedent
import datetime

from hevelli.templates import render



class TemplateMixin:

    def test_template(self):
        for template, result in self.items:
            text = render(template, self.vars)
            self.assertEqual(text, result)



class Multi(TemplateMixin, unittest.TestCase):
  
    vars = {'a':1, 'b': 2, 'c':'zee'}
    items = [
        ('{{a}},{{b}}, {{c}}', '1,2, zee'),
        ('{{b}},{{a}}', '2,1'),
        ('   ', '   '),
        ('{{c}}', 'zee'),
        ('qwer{a}tzui', 'qwer{a}tzui')
        ]

class Single(TemplateMixin, unittest.TestCase):
  
    vars = {'test':123.45}
    items = [
        ('{{test}}', '123.45')
        ]


class Filters(TemplateMixin, unittest.TestCase):
    vars = {'bin': '/usr/local/bin',
            'root': '~root',
            }
    items = [
        ('{{bin|filename}}', 'bin'),
        ('{{bin|dirname}}', '/usr/local'),
        ('{{root|expanduser}}', '/root'),
        ]
    
        
class DefaultVars(TemplateMixin, unittest.TestCase):

    def setUp(self):
        # monkey patch datetime.datetime.now()
        self.now = datetime.datetime(2000, 7, 31, 12, 8, 9)
        class dt:
            @staticmethod
            def now(): return self.now
        self.old_datetime = datetime.datetime
        datetime.datetime = dt

    def tearDown(self):
        datetime.datetime = self.old_datetime

    vars = {}
    items = [
        ('{{YEAR}}', '2000'),
        ('{{MONTH}}', '7'),
        ('{{DAY}}', '31'),
        ('{{ISODATE}}', '2000-07-31'),
        ('{{HOUR}}', '12'),
        ('{{MINUTE}}', '8'),
        ('{{SECOND}}', '9'),
        ('{{ISOTIME}}', '12:08:09'),
        ('{{ISODATETIME}}', '2000-07-31T12:08:09'),
        ('{{CTIME}}', 'Mon Jul 31 12:08:09 2000'),
        ]
        

        


