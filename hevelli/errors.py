# -*- coding: utf-8 -*-

class HevelliException(RuntimeError): pass

class TemplateError(HevelliException): pass
    
class WorldError(HevelliException): pass

    
class ParserError(HevelliException):

    def __init__(self, inputsrc, msg, line_num=None):
        self.inputsrc = inputsrc
        self.msg = msg
        self.line_num = line_num

    def __str__(self):
        if self.line_num is None:
            line_num = self.inputsrc.line_num
        else:
            self.line_num
        return "Error in '{file}' line {line_num}:\n  {line}\n{msg}".format(
            file=self.inputsrc,
            msg=self.msg,
            line_num=line_num,
            line=self.inputsrc.line
            )
    

                                     
class InvalidSectionNameError(ParserError): pass

class InvalidAttributeError(ParserError): pass
    
class InvalidValueError(ParserError): pass
    
class RequiredAttributeMissingError(ParserError): pass

class InvalidDefaultForChoiceError(ParserError): pass

class FileCreationError(ParserError): pass
    
class ExecError(ParserError):

    def __init__(self, inputsrc, process_exception, line_num=None):
        self.process_exception = process_exception
        try:  #(subprocess.CalledProcessError 
            msg =  "Error running '{cmd}', code {code}:\n{output}".format(
                cmd = self.process_exception.cmd,
                code = self.process_exception.returncode,
                output = self.process_exception.output)
        except AttributeError:  # OSError
             msg =  "{error}".format(
                     error = self.process_exception.strerror)
        ParserError.__init__(self, inputsrc, msg, line_num)
            
        
class ConfigFileError(HevelliException):

    def __init__(self, path, msg):
        self.path = path
        self.msg = msg

    def __str__(self):
        return "Error in config file '{path}': '{msg}'".format(
            path = self.path, msg = self.msg)
