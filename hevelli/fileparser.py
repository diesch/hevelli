# -*- coding: utf-8 -*-

import os

from . import parser, errors, attributes

class FileParser(parser.SectionParser):

    def create_attributes(self):
        return (
            attributes.Attrib('path', required=True),
            attributes.Attrib('text', only_once=False, is_multiline=True),
            attributes.Attrib('copy', only_once=False),
            attributes.Attrib('template', only_once=False),
            attributes.PermAttrib('perm'),
            )


    def _get_text_string(self, value):
        return '\n'.join(value) + '\n'

    def _get_copy_string(self, value):
        with open(value[0]) as inpt:
            return inpt.read()
          
    def _get_template_string(self, value):
        with open(value[0]) as inpt:
            content = inpt.read()
        return self.world.render_template(content)


    def _get_output_file(self, path): # for easier testing
        return open(path, 'w')  
          
    def on_end(self):
          path = self.get_attrib_value('path')
          perm = self.get_attrib_value('perm')

          try:
              with self._get_output_file(path) as outpt:
                for d in self.data:
                    key = d['key']
                    value = d['value']
                    if key == 'text':
                        outpt.write(self._get_text_string(value))
                    elif key == 'copy':
                        outpt.write(self._get_copy_string(value))
                    elif key == 'template':
                        outpt.write(self._get_template_string(value))
          except IOError as e:
              msg = 'File "{file}": {error}'.format(
                  file=e.filename,
                  error=str(e))
              raise errors.FileCreationError(self.inputsrc, msg)
          if perm != '':
                os.chmod(path, int(perm))
