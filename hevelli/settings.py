# -*- coding: utf-8 -*-
#
# Hevelli - Template system for file system trees
# http://www.florian-diesch.de/software/hevelli/
#
# Copyright (C) 2017 Florian Diesch <devel@florian-diesch.de>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, os.path, subprocess

from hevelli import _meta


APP_TITLE = _meta.TITLE
APP_NAME = _meta.NAME
APP_VERSION = _meta.VERSION
APP_DESC = _meta.DESC
APP_AUTHOR = _meta.AUTHOR_NAME
APP_AUTHOR_EMAIL = _meta.AUTHOR_EMAIL
APP_TIMESTAMP = _meta.TIMESTAMP
APP_YEAR = 2017

app_name = APP_NAME.lower()


CONFIG_DIR = '.hevelli'
CONFIG_ROOT_FILE = 'ROOT'
ATTRIBUTES_FILE = 'attributes.json'


def _get_env_var(name, default=''):
    path = os.environ.get(name, default)
    if path == '':
        return default
    else:
        return path

def _user_path(*dirs):
    path = os.path.join('~', *dirs)
    return os.path.expanduser(path)


XDG_DATA_HOME   = _get_env_var('XDG_DATA_HOME',
                               _user_path('.local', 'share'))
XDG_CONFIG_HOME = _get_env_var('XDG_CONFIG_HOME',
                               _user_path('.config'))
XDG_DATA_DIRS   = _get_env_var('XDG_DATA_DIRS',
                               ' /usr/local/share/:/usr/share/')
XDG_CONFIG_DIRS = _get_env_var('XDG_CONFIG_DIRS',
                               '/etc/xdg')
XDG_CACHE_HOME  = _get_env_var('XDG_CACHE_HOME',
                               _user_path('.cache'))
XDG_RUNTIME_DIR = _get_env_var('XDG_RUNTIME_DIR',
                               None)


GETTEXT_DOMAIN=app_name 

USER_CONFIG_DIR = os.path.join(XDG_CONFIG_HOME, app_name )
try:
    os.makedirs(USER_CONFIG_DIR)
except OSError:
    pass

USER_DATA_DIR = os.path.join(XDG_DATA_HOME, app_name )
try:
    os.makedirs(USER_DATA_DIR)
except OSError:
    pass

USER_CACHE_DIR = os.path.join(XDG_CACHE_HOME, app_name )
try:
    os.makedirs(USER_CACHE_DIR)
except OSError:
    pass


DATA_DIR = os.path.join('/', 'usr', 'share', app_name )


LOCALE_DIR='/usr/share/locale'

WEB_URL = _meta.WEB_URL

PAYPAL_URL = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DJCGEPS4746PU'

TRANSLATIONS_URL = 'https://translations.launchpad.net/%s' % app_name
SOURCE_URL = 'https://code.launchpad.net/%s' % app_name

BUGREPORT_URL = 'https://bugs.launchpad.net/%s/+filebug' % app_name

