# -*- coding: utf-8 -*-
#
# Hevelli - Template system for file system trees
# http://www.florian-diesch.de/software/hevelli/
#
# Copyright (C) 2017 Florian Diesch <devel@florian-diesch.de>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, argparse

from .ui import SimpleTextUI
from .world import World
from .inputsources import InputSource
from . import settings, parser, varparser, fileparser, errors, msgparser, runparser


def do_use(args):
    ui = SimpleTextUI()
    world = World(ui)
    world.load_default_attributes()
    
    sections = {'var': varparser.VarParser,
                'file': fileparser.FileParser,
                'msg': msgparser.MsgParser,
                'run': runparser.RunParser,
    }
    
    p = parser.RecipeParser(world, sections)
    try:
        with InputSource(args.recipe) as inpt:
            p.parse(inpt)
    except errors.HevelliException as e:
        print('\n\n')
        print('*'*35)
        print(e)
        print('*'*35)
        sys.exit(1)
        print(world.vars)
        world.save_attributes()

        
def do_list(args):
    print('LIST', args)

def parse_cli_args():
    parser = argparse.ArgumentParser(
        prog=settings.app_name,
        allow_abbrev=True)
    
    parser.add_argument('--version', action='version',
          version='{} {}'.format(settings.APP_TITLE, settings.APP_VERSION))
    
    subparsers = parser.add_subparsers()
    
    parser_use = subparsers.add_parser('use')
    parser_use.set_defaults(func=do_use)
    parser_use.add_argument(
        'recipe', help='Recipe to use')

    parser_list = subparsers.add_parser('list')
    parser_list.set_defaults(func=do_list)

    args = parser.parse_args()
    args.func(args)


def main():
    args = parse_cli_args()

