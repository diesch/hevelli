#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from . import errors

class InputSource:

    def __init__(self, file):
        self.file = file
        self.line_num = None
        self.line = None


    def open(self, path_or_file):
        self.line_num = 0
        if hasattr(path_or_file, 'read'):
            return path_or_file
        else:
            try:
                return open(path_or_file)
            except Exception as e:
                raise errors.HevelliException(str(e))

        
    def __enter__(self):
        self.src = self.open(self.file)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.src.close()
        
    def __next__(self):
        for self.line in self.src:
            self.line_num += 1
            self.line = self.line.strip()
            if self.line != '' and self.line [0] != '#':
                return self.line
        raise StopIteration
    
    def __iter__(self):
        return self


    def __str__(self):
        if hasattr(self.file, 'read'):
            return self.file.__class__.__name__
        else:
            return self.file
