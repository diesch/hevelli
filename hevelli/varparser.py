# -*- coding: utf-8 -*-

from . import parser, errors, attributes


class VarParser(parser.SectionParser):

    def create_attributes(self):
        return (
            attributes.Attrib('name', required=True),
            attributes.Attrib('value', is_multiline=True),
            attributes.FixedValueAttrib('ask', default='auto',
                values=('never', 'always', 'auto'), is_multiline=True),
            attributes.Attrib('question', is_multiline=True),
            attributes.Attrib('choice', num_args=2, is_multiline=True),
        )
        

    def _get_question_msg(self, question, name, value):
        if question == '':
            question = 'Value for "{}"'.format(name)
        return question
        

    def _maybe_ask(self, value):
        question = self.get_attrib_value('question')
        ask = self.get_attrib_value('ask')
        name =  self.get_attrib_value('name')
        if ask != 'never' and not (ask == 'auto' and value != ''):
            msg = self._get_question_msg(question, name, value)
            choice = self.get_attrib_value('choice', as_string=False)
            if choice == '':
                value = self.world.ask(msg, default=value)
            else:
                if not self._is_value_valid_for_choice(value, choice):
                    msg = 'Value "{}" not in choice'.format(value)
                    raise errors.InvalidDefaultForChoiceError(self.inputsrc, msg)
                value = self.world.choice(msg, choice, default=value)
        return value

    
    def _is_value_valid_for_choice(self, value, choice):
        return value in {x[1] for x in choice}
                
    def on_end(self):
        name = self.get_attrib_value('name')
        value = self.get_attrib_value('value')
        
        if value == '':
            value = self.world.get_var(name, '')

        value = self._maybe_ask(value)
        self.world.set_var(name, value)
            
 
