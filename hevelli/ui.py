#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pprint import pprint


class SimpleTextUI(object):

    def ask(self, msg, default=None):
        msg = '{q} [default: {default}] '.format(
            q=msg,
            default=default if default else '')
        result = self._input(msg)
        if default is not None and result=='':
            return default
        return result

    def confirm(self, msg):
        msg = '{}? [y/n] '.format(msg)
        answer = None
        while answer not in ('y', 'n'):
            answer = self._input(msg).lower()
        return answer == 'y'
            

    def _print(self, *args):
        print(*args)

    def _input(self, msg):
        return input(msg)

    def text(self, msg):
        self._print(msg)
        
    def choice(self, msg, items, default=None):
        default_str = None
        while True:
            self._print(msg+'\n')
            for n, (text, value) in enumerate(items):
                self._print('  {n})  {text}'.format(n=n, text=text))
                if value == default:
                    default_str = text
            prompt = "\n Your choice: "
            if default_str is not None:
                prompt = prompt + '[default: {}] '.format(default_str)
            inpt = self._input(prompt)
            try:
                if inpt == '' and default is not None:
                    return default
                else:
                    i = int(inpt)
                    if 0 <= i < len(items):
                        return items[i][1]
                    else:
                        raise ValueError()
            except ValueError:
                self._print('Invalid choice: "{}"\n\n'.format(inpt))
                
                            
