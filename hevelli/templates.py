# -*- coding: utf-8 -*-


import jinja2
import datetime, os.path

def render(template, vars):
    env = jinja2.Environment()
    env.filters['filename'] = os.path.basename
    env.filters['dirname'] = os.path.dirname
    env.filters['expanduser'] = os.path.expanduser

    tpl = env.from_string(template)
    default_vars = _get_default_vars()
    default_vars.update(vars)
    return tpl.render(**default_vars)

def _get_default_vars():
    now = datetime.datetime.now().replace(microsecond=0)
    vars = {
        'YEAR': now.year,
        'MONTH': now.month,
        'DAY': now.day,                 
        'ISODATE': now.date().isoformat(),
        'HOUR': now.hour,
        'MINUTE': now.minute,
        'SECOND': now.second,
        'ISOTIME': now.time().isoformat(),
        'ISODATETIME': now.isoformat(),
        'CTIME': now.ctime(),
        }
    return vars
