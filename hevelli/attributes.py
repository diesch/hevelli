# -*- coding: utf-8 -*-

import re

class Attrib:

    def __init__(self, name, num_args=1, required=False,
                     is_multiline=False, default='', only_once=True):
        self.name = name
        self.num_args = num_args
        self.required = required
        self.is_multiline = is_multiline
        self.default = default
        self.only_once = only_once
        self.regex = re.compile(self.create_regex())

    def create_regex(self):
        return r'(["\'].*["\'])\s*' * self.num_args

    def parse(self, value, world):
        match = self.regex.match(value)
        if match is None:
            raise ValueError('Invalid value for "{name}".'.format(
                name=self.name))
        else:
            args = self.convert(match.groups(), world)
            if self.num_args > 1: 
                return args
            else:
                return args[0]

    def maybe_render_template(self, s, world):
        if s[0] != s[-1]:
            raise ValueError("Parens don't match.")
        if s[0] == '"':
            return world.render_template(s[1:-1])
        elif s[0] == "'":
            return s[1:-1]
        else:
            raise ValueError("Invalid parens.")
        
    def convert(self, args, world):
        return [self.maybe_render_template(a, world) for a in args]


class FixedValueAttrib(Attrib):
    
    def __init__(self, name, values, *args, **kwargs):
        Attrib.__init__(self, name=name, *args, **kwargs)
        self.fixed_values = values

    def convert(self, args, world):
        args = Attrib.convert(self, args, world)
        
        if all(a in self.fixed_values for a in args):
            return args
        else:
            raise ValueError("Invalid value for '{name}'.".format(
                name=self.name))
    

class BoolAttrib(FixedValueAttrib):

    def __init__(self, name, *args, **kwargs):
        default = kwargs.get('default', 'on')
        del kwargs['default']
        FixedValueAttrib.__init__(
            self, name=name, values={'yes', 'no', 'on', 'off', 'true', 'false'},
            default=default,
        *args, **kwargs)

    def convert(self, *args, **kwargs):
        args = FixedValueAttrib.convert(self, *args, **kwargs)
        ret =  [a in {'on', 'yes', 'true'} for a in args]
        return ret
        
class PermAttrib(Attrib):
    
    def convert(self, args, world):
        args = Attrib.convert(self, args, world)
        result = []
        for a in args:
            try:
                x = int(a, 8)
                if not 0<= x <= int('777', 8):
                    raise ValueError()
                result.append(x)
            except ValueError:
                raise ValueError('Invalid permission "{}"'.format(a))
        return result
