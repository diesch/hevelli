# -*- coding: utf-8 -*-

from . import templates, errors, utils, settings


class World:

    def __init__(self, ui):
        self.ui = ui
        self.vars = {}
        self.vars_to_save = set()


    def _set_default_vars_from_dict(self, adict):
        for k,v in adict.items():
            self.set_var(k, v, save=False)
        
    def load_default_attributes(self):
        for f in utils.get_config_files(settings.ATTRIBUTES_FILE):
            d = utils.load_from_json_file(f, {})
            self._set_default_vars_from_dict(d)

    def save_attributes(self):
        vars = {key:self.vars[key] for key in self.vars_to_save}
        path = os.path.join(settings.CONFIG_DIR, settings.ATTRIBUTES_FILE)
        utils.write_to_json_file(path, vars)
        
            
    def get_var(self, var, default=None):
        return self.vars.get(var, default)
    
    def set_var(self, var, value, save=True):
        if var == '':  # shoudn't happen
            raise errors.WorldError("Invalid var name: ''")
        
        if save:
            self.vars_to_save.add(var)
        self.vars[var] = value

    def render_template(self, template):
        return templates.render(template, self.vars)

    
    def ask(self, msg, default=None):
        if self.ui is None:
            return default
        else:
            return self.ui.ask(msg, default)
        
    def choice(self, msg, items, default=None):
        if self.ui is None:
            return default
        else:
            return self.ui.choice(msg, items, default)
        

        
