# -*- coding: utf-8 -*-
import subprocess

from . import parser, errors, attributes


class RunParser(parser.SectionParser):

    def create_attributes(self):
        return (
            attributes.Attrib('cmd', only_once=True, is_multiline=True),
            attributes.BoolAttrib('quiet', only_once=True, default='False')

            )


    def on_end(self):
        cmd = self.get_attrib_value('cmd')
        quiet = self.get_attrib_value('quiet') == 'True'
        try:
            if quiet:
                subprocess.check_output(cmd, shell=True)
            else:
                subprocess.check_call(cmd, shell=True)
        except  (subprocess.CalledProcessError, OSError) as e:
            raise errors.ExecError(self.inputsrc, e)
