#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re, subprocess

from . import  errors, attributes, utils

class RecipeParser:

    def __init__(self, world, section_parsers):
        self.world = world
        self.section_parsers = section_parsers

    def parse(self, inputsrc):
        for line in inputsrc:
            tokens = line.split()
            try:
                parser_factory = self.section_parsers[tokens[0]]
            except (IndexError, KeyError):                 
                raise errors.InvalidSectionNameError(
                    inputsrc, "Invalid section name.")
            parser = parser_factory(inputsrc, self.world)
            parser.parse()


class SectionParser:

    def __init__(self, inputsrc, world):
        self.inputsrc = inputsrc
        self.world = world
        self.data = []
        self.keys = set()
        attribs = self.create_attributes()
        self.attributes = {a.name:a for a in attribs}
        self.defaults = {a.name:a.default for a in attribs}
        self.line_regex = re.compile(
            r'\s*(?P<key>\w+)\s+(?P<op>[=<])\s+(?P<value>.*)\s*')

    def create_attributes(self):
        return []

    def _add_data(self, attrib, op, value):
        try:
            value = attrib.parse(value, self.world)
        except ValueError as e:
            raise errors.InvalidValueError(self.inputsrc, str(e))

        if op == '<':
            value = utils.run_shell_command(value, self.inputsrc)
            
        if (len(self.data) > 0 and
            self.data[-1]['key'] == attrib.name and
            attrib.is_multiline):
            self.data[-1]['value'].append(value)
        else:
            if attrib.only_once and attrib.name in self.keys:
                msg = 'Attribute "{name}" only allowed once'.format(
                    name=attrib.name)
                raise errors.InvalidAttributeError(self.inputsrc, msg)
            self.data.append(
                {'key': attrib.name,
                 'value': [value],
                 }
                )
        self.keys.add(attrib.name)
        
    def parse_line(self, line):
        match = self.line_regex.fullmatch(line)
        if match is None:
            raise errors.ParserError(self.inputsrc, 'Invalid line.')
        
        key, op, value = match.group('key', 'op', 'value')
        
        try:
            attrib = self.attributes[key]        
        except KeyError: 
            raise errors.InvalidAttributeError(
                self.inputsrc, 'Unknown attribute: "{}"'.format(key))
        
        self._add_data(attrib, op, value)
                
            
    def parse(self):
        line = '' # avoid later error if inputsrc is empty
        for line in self.inputsrc:
            if line.strip() == 'end':
                self.check_required_attributes()
                self.on_end()
                break
            else:
                self.parse_line(line)
        if line.strip() != 'end': #EOF without 'end'
            raise errors.ParserError(self.inputsrc, "No 'end' found.")

    def get_attrib_value(self, name, index=0, as_string=True):
        try:
            result = [x for x in self.data if x['key'] == name][index]['value']
            if as_string:
                return '\n'.join(str(i) for i in result)
            else:
                return result
        except IndexError:
            return self.defaults[name]
        
    def get_missing_attributes(self):
        req = {a.name for a in self.attributes.values() if a.required}
        return req - self.keys
        
    def check_required_attributes(self):
        missing = self.get_missing_attributes()
        if missing:
            mstr = ', '.join(missing)
            raise errors.RequiredAttributeMissingError(
                self.inputsrc,
                'Missing required attributes: {}'.format(mstr)
            )
        
    def on_end(self):
        pass
