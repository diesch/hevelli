# -*- coding: utf-8 -*-

import subprocess, os, glob, json

from . import errors, settings

def run_shell_command(cmd, inputsrc):
    try:
        return subprocess.check_output(
        cmd,
        stderr=subprocess.STDOUT,
        shell=True).decode()
    except subprocess.CalledProcessError as e:
        raise errors.ExecError(inputsrc, e)



def get_config_dirs(dir=None):
    dirs = []
    if dir is None:
        dir = os.getcwd()
    else:
        dir = os.path.join(os.getcwd(), dir)

    while True:
        cfgdir = os.path.join(dir, settings.CONFIG_DIR)
        if os.path.isdir(cfgdir):
            dirs.append(cfgdir)

        root_file = os.path.join(cfgdir, settings.CONFIG_ROOT_FILE)
        if dir=='/' or os.path.isfile(root_file):
            break

        dir = os.path.normpath(os.path.join(dir, '..'))
    dirs.reverse()
    return dirs


def get_config_files(file_glob, dir=None):
    files = []
    config_dirs = get_config_dirs(dir)
    for d in config_dirs:
        cfgfiles = glob.glob(os.path.join(d, file_glob))
        if cfgfiles:
            files.extend(cfgfiles)
    return files


def load_from_json_file(path, default=None):
    if not os.path.exists(path):
        return default

    with open(path) as input:
        try:
            return json.load(input)
        except ValueError as e:
            raise errors.ConfigFileError(path, str(e))


def write_to_json_file(path, data):
    dir = os.path.dirname(path)
    create_dirs(dir)
    try:
        with open(path, 'w') as output:
            json.dump(data, output, sort_keys=True, indent=4)
    except IOError as e:
        raise errors.ConfigFileError(path, str(e))

          
