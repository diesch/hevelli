# -*- coding: utf-8 -*-

from . import parser, errors, attributes

class MsgParser(parser.SectionParser):

    def create_attributes(self):
        return (
            attributes.Attrib('text', only_once=False, is_multiline=True),
            )

    def on_end(self):
        for d in self.data:
            self.world.ui.text('\n'.join(d['value']) + '\n')
